/*
 * program.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "program.h"

#include "log.h"
#include "utils.h"

#include <sys/stat.h>



struct _ln_program
{
	gchar* exec;
	gchar* args;
	gchar* id;
};



static gboolean _is_valid(const gchar* path)
{
	g_assert(path);

	gboolean status = FALSE;

	status = g_file_test(path, G_FILE_TEST_IS_REGULAR);
	if (status)
	{
		if (g_str_has_suffix(path, ".msi"))
		{
			status = FALSE;
			ERR("%s: No supported file", path);
		}
	}
	else
	{
		ERR("%s: No file", path);
	}

	return status;
}


static LnProgram* ln_program_new(const gchar* exec, const gchar* args, const gchar* id)
{
	g_assert(exec);

	LnProgram* self = NULL;

	self = (LnProgram*) g_malloc0(sizeof(LnProgram));
	self->exec = g_strdup(exec);
	self->args = g_strdup(args);
	self->id = g_strdup(id);

	return self;
}


LnProgram* ln_program_create(const gchar* exec, const gchar* args, const gchar* id)
{
	g_assert(exec);

	LnProgram* self = NULL;

	if (_is_valid(exec))
	{
		TRACE("Using executable: %s", exec);
		TRACE("Using arguments: %s", args);
		TRACE("Using program id: %s", id);

		self = ln_program_new(exec, args, id);
	}

	return self;
}


void ln_program_free(LnProgram* self)
{
	g_assert(self);

	g_free(self->exec);
	g_free(self->args);
	g_free(self->id);

	g_free(self);
}


gchar* ln_program_get_cache_path(const LnProgram* self)
{
	g_assert(self);

	g_autofree gchar* xdg_cache = NULL;
	gchar* path = NULL;

	xdg_cache = ln_get_user_cache_path();
	if (xdg_cache)
	{
		path = g_build_path(G_DIR_SEPARATOR_S, xdg_cache, self->id, NULL);
		if (path)
		{
			g_mkdir_with_parents(path , 0700);
		}
	}

	return path;
}


gchar* ln_program_get_dirname(const LnProgram* self)
{
	g_assert(self);

	return g_path_get_dirname(self->exec);
}


gchar* ln_program_get_command(const LnProgram* self)
{
	g_assert(self);

	g_autofree gchar* exec = g_path_get_basename(self->exec);

	if (self->args == NULL)
	{
		return g_strdup(exec);
	}

	return g_strdup_printf("%s,%s", exec, self->args);
}


const gchar* ln_program_get_executable(const LnProgram* self)
{
	g_assert(self);

	return self->exec;
}


const gchar* ln_program_get_id(const LnProgram* self)
{
	g_assert(self);

	return self->id;
}