/*
 * log.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __LOG__
#define __LOG__

#include <glib.h>


#define ERR(...)   ln_log("err", __VA_ARGS__)
#define INFO(...)  ln_log("info", __VA_ARGS__)
#define WARN(...)  ln_log("warn", __VA_ARGS__)
#define TRACE(...) ln_trace(__FILE__, __LINE__, __VA_ARGS__)



void ln_log(const gchar* level, const gchar* format, ...);

void ln_trace(const gchar* file, const gint line, const gchar* format, ...);



#endif