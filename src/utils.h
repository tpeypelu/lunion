/*
 * utils.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __UTILS__
#define __UTILS__


#include <glib.h>
#include <glib/gstdio.h>



void ln_append_env(const char* name, const char* value, const char* separator);

GStrv ln_build_argv(const gchar* bin, const gchar* format, ...);

gchar* ln_get_compatdata_path(void);

gchar* ln_get_dirname_bin(void);

gchar* ln_get_exec(gchar* exec);

gchar* ln_get_kernel(void);

gchar* ln_get_output_cmd(const gchar* cmd);

gchar* ln_get_kernel(void);

gchar* ln_get_user_cache_path(void);

gchar* ln_get_user_config_path(void);

gchar* ln_get_user_data_path(void);

gchar* ln_get_user_runtime_path(void);

gchar* ln_get_env(const gchar* suffix_key);

void ln_prepend_env(const char* name, const char* value, const char* separator);

gchar* ln_remove_linefeed_to_end_string(gchar** string);

gboolean ln_reset_env(const gchar* key, const gchar* value);

gboolean ln_run_process(const gchar* workdir, gchar** argv, const gboolean silent);



#endif