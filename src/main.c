/*
 * main.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "driver.h"
#include "lib.h"
#include "log.h"
#include "parser.h"
#include "prefix.h"
#include "program.h"
#include "session.h"
#include "translayer.h"
#include "utils.h"

#include <glib.h>



static gboolean _init_application(gint argc, gchar** argv, gchar** id, gchar** exec)
{
	g_assert(argv);
	g_assert(! *id);
	g_assert(! *exec);

	gboolean status = FALSE;

	if (argc == 2)
	{
		*exec = g_canonicalize_filename(argv[1], NULL);
		if (*exec && g_file_test(*exec, G_FILE_TEST_IS_REGULAR))
		{
			*id = ln_get_env("program_id");
		}
		else
		{
			*id = g_strdup(argv[1]);

			g_free(*exec);
			*exec = NULL;
		}

		status = TRUE;
	}

	return status;
}


int main(int argc, char* argv[])
{
	gint status = 1;
	g_autofree gchar* id = NULL;
	g_autofree gchar* exec = NULL;

	if (_init_application(argc, argv, &id, &exec))
	{
		const gchar* prefix_path = NULL;
		g_autofree gchar* args = NULL;
		g_autoptr(LnParser) cfg = NULL;
		g_autoptr(LnLib) dxvk = NULL;
		g_autoptr(LnLib) vkdp = NULL;
		g_autoptr(LnProgram) program = NULL;
		g_autoptr(LnWine) wine = NULL;

		cfg = ln_parser_create();

		if (! exec)
		{
			exec = ln_parser_get_string(cfg , id, "command");
			if (! exec)
			{
				return status;
			}
		}
		args = ln_parser_get_string(cfg , id , "command_args");

		if (! (program = ln_program_create(exec, args, id)))
		{
			return status;
		}

		if (! (wine = ln_parser_get_wine(cfg, id)))
		{
			return status;
		}

		dxvk = ln_translayer_get_dxvk(cfg, id);
		vkdp = ln_translayer_get_vkd3dproton(cfg, id);

		INFO("Preparing to launch...");
		prefix_path = g_getenv("WINEPREFIX");
		if (! prefix_path)
		{
			g_autofree gchar* path = NULL;

			if ((path = ln_prefix_build_path(id)))
			{
				if (g_setenv("WINEPREFIX", path, TRUE))
				{
					prefix_path = g_getenv("WINEPREFIX");
				}
			}
		}
		INFO("Using wineprefix from \"%s\"", prefix_path);

		if (ln_wine_make_prefix(wine, NULL))
		{
			g_autoptr(LnPrefix) pfx = NULL;

			if ((pfx = ln_prefix_create()))
			{
				LnDriver driver = LN_DRIVER_NULL;

				ln_prefix_setup(pfx, dxvk, vkdp);
				driver = ln_parser_get_driver(cfg);

				ln_session_init(wine, program, driver);

				INFO("Starting...");
				status = ln_session_run(wine, program);

				ln_session_exit(wine);
			}
		}
	}

	return status;
}