/*
 * lib.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "lib.h"

#include "log.h"



struct _ln_lib
{
	gchar* path64;
	gchar* path32;
};



void ln_lib_free(LnLib* self)
{
	g_assert(self);

	g_free(self->path64);
	g_free(self->path32);

	g_free(self);
}


const gchar* ln_lib_get_path(const LnLib* self, LnArch arch)
{
	const gchar* ret = NULL;

	if (arch == LN_ARCH_64BIT_MODE)
	{
		ret = self->path64;
	}

	if (arch == LN_ARCH_32BIT_MODE)
	{
		ret = self->path32;
	}

	return ret;
}


LnLib* ln_lib_new(const gchar* path32, const gchar* path64)
{
	LnLib* self = NULL;

	self = (LnLib*) g_malloc0(sizeof(LnLib));

	if (path32)
	{
		self->path32 = g_strdup(path32);
	}

	if (path64)
	{
		self->path64 = g_strdup(path64);
	}

	return self;
}


void ln_lib_setup_env(const gchar* key, const LnLib* self)
{
	g_assert (key);
	g_assert (self);

	if (self->path64)
	{
		ln_append_env(key, self->path64, ":");
	}
	if (self->path32)
	{
		ln_append_env(key, self->path32, ":");
	}

	TRACE("%s=%s", key, g_getenv(key));
}