/*
 * utils.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "utils.h"

#include "log.h"

#include <fcntl.h>
#include <libgen.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>



enum _ln_xdg
{
	LN_XDG_NULL            = 0x0,
	LN_XDG_USER_CACHE      = 0x1,
	LN_XDG_USER_CONFIG     = 0x2,
	LN_XDG_USER_DATA       = 0x3,
	LN_XDG_USER_RUNTIME    = 0x4,
	LN_XDG_USER_STATE_FILE = 0x5
};


static gchar*   _get_xdg_user_default_path(enum _ln_xdg type);

static gchar*   _get_xdg_user_path(enum _ln_xdg type);

static gchar*   _ln_xdg_to_string(enum _ln_xdg value);

static void     _print_argv(gchar** argv);


static gchar* _get_xdg_user_default_path(enum _ln_xdg type)
{
	const gchar* home = NULL;
	gchar* path = NULL;

	home = g_getenv("HOME");
	switch (type)
	{
		case LN_XDG_USER_CACHE:
			path = g_build_filename(home, ".cache", NULL);
			break;
		case LN_XDG_USER_CONFIG:
			path = g_build_filename(home, ".config", NULL);
			break;
		case LN_XDG_USER_DATA:
			path = g_build_filename(home, ".local", "share", NULL);
			break;
		case LN_XDG_USER_RUNTIME:
			path = g_build_filename("tmp", NULL);
			break;
		case LN_XDG_USER_STATE_FILE:
			path = g_build_filename(home, ".local", "state", NULL);
			break;
		default:
			ERR("Not follow XDG Base Directory specification");
	}

	return path;
}


static gchar* _get_xdg_user_path(enum _ln_xdg type)
{
	g_autofree gchar* key = NULL;
	g_autofree gchar* base = NULL;
	gchar* path = NULL;

	key = _ln_xdg_to_string(type);
	if (key)
	{
		base = g_strdup(g_getenv(key));
		if (! base)
		{
			base = _get_xdg_user_default_path(type);
		}

		if (base)
		{
			path = g_build_filename(base, "lunion", NULL);
		}
	}

	return path;
}


static gchar* _ln_xdg_to_string(enum _ln_xdg value)
{
	gchar* key = NULL;

	switch (value)
	{
		case LN_XDG_USER_CACHE:
			key = g_strndup("XDG_CACHE_HOME", 14);
			break;
		case LN_XDG_USER_CONFIG:
			key = g_strndup("XDG_CONFIG_HOME", 15);
			break;
		case LN_XDG_USER_DATA:
			key = g_strndup("XDG_DATA_HOME", 13);
			break;
		case LN_XDG_USER_STATE_FILE:
			key = g_strndup("XDG_STATE_HOME", 14);
			break;
		case LN_XDG_USER_RUNTIME:
			key = g_strndup("XDG_RUNTIME_DIR", 15);
			break;
		default:
			ERR("Not follow XDG Base Directory specification");
	}

	return key;
}


static void _print_argv(gchar** argv)
{
	g_assert(argv);

	g_autoptr (GString) msg = NULL;

	msg = g_string_new(NULL);
	if (msg)
	{
		gchar** pt = argv;
		while (*pt)
		{
			g_string_append(msg, *pt);

			if (++pt)
			{
				g_string_append(msg, " ");
			}
		}

		TRACE("%s", msg->str);
	}
}


static void ln_insert_env(const char* name, const char* value, const char* separator, const int pos)
{
	g_assert(name);
	g_assert(value);

	const gchar* env = NULL;

	if ((env = g_getenv(name)))
	{
		g_autofree gchar* tmp = NULL;

		if (pos > 0)
		{
			tmp = g_strconcat(env, separator, value, NULL);
		}
		else if (pos < 0)
		{
			tmp = g_strconcat(value, separator, env, NULL);
		}
		else
		{
			ERR("Failed to add environment variable: %s=%s", name, value);
			TRACE("%s=%s -> %d", name, value, pos);
			return;
		}

		if (tmp)
		{
			g_setenv(name, tmp, TRUE);
		}
	}
	else
	{
		g_setenv(name, value, TRUE);
	}
}


void ln_append_env(const char* name, const char* value, const char* separator)
{
	g_assert(name);
	g_assert(value);

	ln_insert_env(name, value, separator, 1);
}


GStrv ln_build_argv(const gchar* bin, const gchar* first_arg, ...)
{
	g_assert(bin);
	g_assert(first_arg);

	g_autoptr (GStrvBuilder) str = NULL;

	str = g_strv_builder_new();
	if (str)
	{
		gchar* next_arg = NULL;
		va_list args;

		g_strv_builder_add(str, bin);
		g_strv_builder_add(str, first_arg);

		va_start(args, first_arg);
		next_arg = va_arg(args, gchar*);

		while (next_arg)
		{
			g_strv_builder_add(str, next_arg);
			next_arg = va_arg(args, gchar*);
		}

		va_end(args);
	}

	return g_strv_builder_end(str);
}


gchar* ln_get_compatdata_path(void)
{
	gchar* path = NULL;

	g_autofree gchar* usrdata = NULL;

	if ((usrdata = ln_get_user_data_path()))
	{
		path = g_build_path(G_DIR_SEPARATOR_S, usrdata, "compatdata", NULL);
	}

	return path;
}


gchar* ln_get_dirname_bin(void)
{
	g_autofree gchar* real_path = NULL;
	gchar* path = NULL;

	real_path = g_file_read_link("/proc/self/exe", NULL);
	if (real_path)
	{
		path = g_path_get_dirname(real_path);
	}

	return path;
}


/* WARNING Do not pass these pointers to free */
gchar* ln_get_exec(gchar* path)
{
	g_assert(path);

	return basename(path);
}


gchar* ln_get_kernel(void)
{
	gchar* ret = NULL;
	struct utsname buffer;

	if (uname(&buffer))
	{
		ret = g_strjoin(" ",
			buffer.sysname,
			buffer.release,
			buffer.version,
			buffer.machine,
			NULL);
	}

	return ret;
}


gchar* ln_get_output_cmd(const gchar* cmd)
{
	g_assert(cmd);

	gint wait_status;
	gchar* value = NULL;

	g_spawn_command_line_sync(cmd, &value, NULL, &wait_status, NULL);
	g_spawn_check_wait_status(wait_status, NULL);

	if (strlen(value) <= 1)
	{
		g_free(value);
		value = NULL;
	}

	return value;
}


gchar* ln_get_user_cache_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_CACHE);
}


gchar* ln_get_user_config_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_CONFIG);
}


gchar* ln_get_user_data_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_DATA);
}


gchar* ln_get_user_runtime_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_RUNTIME);
}


gchar* ln_get_env(const gchar* suffix_key)
{
	g_assert(suffix_key);

	g_autofree gchar* strup = NULL;
	gchar* value = NULL;

	strup = g_utf8_strup(suffix_key, (gssize) strlen(suffix_key));
	if (strup)
	{
		g_autofree gchar* key = NULL;

		key = g_strconcat("LUNION_", strup, NULL);
		if (key)
		{
			value = g_strdup(g_getenv(key));
		}
		TRACE("%s=%s", key, value);
	}

	return value;
}


static void __attribute__((unused)) ln_log_file(const char* logfile)
{
	if (logfile)
	{
		TRACE("Open log file: %s", logfile);

		int fd = open(logfile, O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);

		if (fd != -1)
		{
			dup2(fd, 1);
			dup2(fd, 2);
			close(fd);
		}
	}
}


void ln_prepend_env(const char* name, const char* value, const char* separator)
{
	g_assert(name);
	g_assert(value);

	ln_insert_env(name, value, separator, -1);
}


gchar* ln_remove_linefeed_to_end_string(gchar** string)
{
	g_assert(*string);

	gint lenght = strlen(*string);

	if ((*string)[lenght - 1] == '\n')
	{
		g_autofree gchar* tmp = NULL;

		tmp = g_strndup(*string, lenght - 1);
		g_free(*string);
		*string = g_strdup(tmp);
	}

	return *string;
}


gboolean ln_reset_env(const gchar* key, const gchar* value)
{
	g_assert(key);

	gboolean status = FALSE;

	if (value)
	{
		status = g_setenv(key, value, TRUE);
	}
	else
	{
		g_unsetenv(key);
		status = TRUE;
	}

	return status;
}


gboolean ln_run_process(const gchar* workdir, gchar** argv, const gboolean silent)
{
	g_assert(argv);

	gint wait_status;
	GSpawnFlags flags = G_SPAWN_DEFAULT;
	g_autoptr(GError) error = NULL;
	gboolean status = FALSE;

	_print_argv(argv);
	flags |= G_SPAWN_SEARCH_PATH_FROM_ENVP;

	if (silent)
	{
		flags |= G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL;
	}

	status = g_spawn_sync(workdir,
		argv,
		NULL,
		flags,
		NULL,
		NULL,
		NULL,
		NULL,
		&wait_status,
		&error);

	if (error)
	{
		g_error_free(error);
		error = NULL;
	}

	status &= g_spawn_check_wait_status(wait_status, &error);

	return status;
}