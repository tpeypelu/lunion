/*
 * wine.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WINE__
#define __WINE__

#include "lib.h"

#include <glib.h>



typedef struct _ln_wine LnWine;


gboolean ln_wine_add_registry_key(const LnWine* self, const gchar* key, const gchar* name, const gchar* type, const gchar* data);

LnWine* ln_wine_create(const gchar* path);

gboolean ln_wine_delete_registry_key(const LnWine* self, const gchar* key, const gchar* name);

void ln_wine_free(LnWine* self);

gchar* ln_wine_get_bin_path(const LnWine* self);

gchar* ln_wine_get_exec(const LnWine* self);

gchar* ln_wine_get_version(const LnWine* self);

void ln_wine_init(const LnWine* self);

gboolean ln_wine_make_prefix(const LnWine* self, const gchar* option);

gboolean ln_wine_use_boot(const LnWine* self, const gchar* option);

gboolean ln_wine_use_server(const LnWine* self, const gchar* option);

gboolean ln_wine_wait_server(const LnWine* self);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(LnWine, ln_wine_free)



#endif