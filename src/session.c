/*
 * session.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "session.h"

#include "log.h"
#include "prefix.h"
#include "translayer.h"
#include "utils.h"


#define SESSION_STATE_ENV_KEY "LUNION_SESSION_STATE_PATH"



static gchar* _build_session_state_filename(const gchar* program_id)
{
	g_assert(program_id);

	g_autofree gchar* xdg_runtime = NULL;
	gchar* file = NULL;

	xdg_runtime = ln_get_user_runtime_path();
	if (xdg_runtime && g_mkdir_with_parents(xdg_runtime, 0700) == 0)
	{
		file = g_build_filename(xdg_runtime, program_id, NULL);
	}

	return file;
}


static gboolean _save_session_state(const gchar* file)
{
	g_assert(file);

	const gchar* path = NULL;
	g_autoptr (GError) err = NULL;
	gboolean status = FALSE;

	path = ln_prefix_get_path();
	if (path)
	{
		status = g_file_set_contents(file, path, (gsize) strlen(path), &err);
		if (! status)
		{
			ERR("%s", err->message);
		}
	}

	if (status)
	{
		TRACE("Saving session state into %s", file);
		g_setenv(SESSION_STATE_ENV_KEY, file, TRUE);
	}

	return status;
}


void ln_session_save(const gchar* program_id)
{
	g_assert(program_id);

	g_autofree gchar* file = NULL;

	file = _build_session_state_filename(program_id);
	if (file)
	{
		_save_session_state(file);
	}
}


void ln_session_init(const LnWine* wine, const LnProgram* program, const LnDriver driver)
{
	g_assert(wine);
	g_assert(program);

	g_autofree gchar* path = NULL;

	path = ln_program_get_cache_path(program);
	if (path)
	{
		ln_session_save(ln_program_get_id(program));

		ln_wine_init(wine);
		ln_translayer_init(DXVK, path);
		ln_translayer_init(VKD3DPROTON, path);
		ln_driver_init(driver, path);
	}
}


void ln_session_exit(const LnWine* wine)
{
	g_assert(wine);

	const gchar* file = NULL;
	gint status = -1;

	ln_wine_wait_server(wine);

	file = g_getenv(SESSION_STATE_ENV_KEY);
	if (file)
	{
		status = g_remove(file);
	}

	if (status != 0)
	{
		ERR("Cannot remove to session state file: %s");
	}
}


gint ln_session_run(const LnWine* wine, const LnProgram* program)
{
	g_assert(wine);
	g_assert(program);

	g_autofree gchar* command = NULL;
	gint status = -1;

	command = ln_program_get_command(program);
	if (command)
	{
		g_autofree gchar* tmp = NULL;
		g_autofree gchar* bin = NULL;

		bin = ln_wine_get_bin_path(wine);
		tmp = g_strdup_printf("%s,%s", bin, command);
		if (tmp)
		{
			g_auto (GStrv) argv = NULL;

			argv = g_strsplit(tmp, ",", -1);
			if (argv)
			{
				g_autofree gchar* path = NULL;

				path = ln_program_get_dirname(program);
				if (path)
				{
					TRACE("Using working directory: %s", path);
					status = ln_run_process(path, argv, FALSE);
				}
				else
				{
					ERR("%s: No such file or directory.", path);
				}
			}
		}
	}

	return  status;
}