/*
 * wine.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "wine.h"

#include "arch.h"
#include "log.h"
#include "parser.h"
#include "utils.h"

#include <glib/gstdio.h>



struct _ln_wine
{
	LnArch bin;
	LnArch bin64;
	gchar* path;
};



static LnArch   _get_executable_arch(const gchar* path, const gchar* name);

static gboolean _is_valid           (const gchar* path);

static gboolean _is_wow64           (const LnWine* self);

static gboolean _test_executable    (const gchar* path, const gchar* filename);


static LnArch _get_executable_arch(const gchar* path, const gchar* name)
{
	g_assert(path);
	g_assert(name);

	g_autofree gchar* exec = NULL;
	LnArch arch = LN_ARCH_NULL;

	exec = g_build_filename(path, name, NULL);
	if (exec)
	{
		gboolean is_exec = FALSE;

		is_exec = g_file_test(exec, G_FILE_TEST_IS_EXECUTABLE) && ! g_file_test(exec , G_FILE_TEST_IS_SYMLINK);
		if (is_exec)
		{
			arch = ln_arch_get_from_binary_file(exec);
		}
	}

	return arch;
}


static gboolean _is_valid(const gchar* path)
{
	g_assert(path);

	gboolean status = FALSE;

	if (g_file_test(path, G_FILE_TEST_IS_DIR))
	{
		status = _test_executable(path, "wineboot")
			     && _test_executable(path, "wineserver");
	}

	if (! status)
	{
		ERR("Not a valid directory: %s", path);
	}

	return status;
}


static gboolean _is_wow64(const LnWine* self)
{
	g_assert(self);

	gboolean status = FALSE;

	/* Experimental wow64 mode since 8.0+ */
	if (self->bin == LN_ARCH_64BIT_MODE && self->bin64 == LN_ARCH_NULL)
	{
		status = TRUE;
	}

	/* Old wow64 mode */
	if (self->bin == LN_ARCH_32BIT_MODE && self->bin64 == LN_ARCH_64BIT_MODE)
	{
		status = TRUE;
	}

	return status;
}


static gboolean _test_executable(const gchar* path, const gchar* filename)
{
	g_assert(path);
	g_assert(filename);

	g_autofree gchar* exec = NULL;
	gboolean status = FALSE;

	exec = g_build_filename(path, filename, NULL);
	if (exec)
	{
		status = g_file_test(exec, G_FILE_TEST_IS_EXECUTABLE);
	}

	if (! status)
	{
		ERR("%s: No such executable", exec);
	}

	return status;
}


/* TODO Need rework for detecting GStreamer implementation (wine-ge builds)
static void ln_wine_set_gstreamer_env(const gchar* lib_dir)
{
	g_assert(lib_dir);

	g_autofree gchar* path = NULL;

	path = g_build_path(lib_dir, "gstreamer-1.0", NULL);
	if (path && g_file_test(path, G_FILE_TEST_IS_DIR))
	{
		ln_append_env("GST_PLUGIN_SYSTEM_PATH_1_0", path, ":");
	}
}


static void ln_wine_configure_gstreamer_runtime(const LnWine* self)
{
	g_assert(self);

	const gchar* lib = NULL;

	lib = ln_lib_get_path(self->lib_dir, LN_ARCH64);
	if (lib)
	{
		ln_wine_set_gstreamer_env(lib);
	}

	lib = ln_lib_get_path(self->lib_dir, LN_ARCH32);
	if (lib)
	{
		ln_wine_set_gstreamer_env(lib);
	}
}
*/


/* TODO Need rework this functions for don't depend to GKeyFile
static void ln_wine_use_debug(GKeyFile* cfg, const gchar* group)
{
	g_assert(group);

	g_autofree gchar* debug = NULL;

	debug = ln_parser_get_string(cfg, group, "use_winedbg");
	if (debug && ! g_strcmp0(debug, "true"))
	{
		ln_append_env("WINEDLLOVERRIDES", "winedebug.exe=d", ";");
	}
}
*/


/* TODO Need rework this functions for don't depend to GKeyFile
static void ln_wine_use_menubuilder(GKeyFile* cfg, const gchar* group)
{
	g_assert(group);

	g_autofree gchar* symlink = NULL;

	symlink = ln_parser_get_string(cfg, group, "use_winemenubuilder");
	if (symlink && ! g_strcmp0(symlink, "true"))
	{
		ln_append_env("WINEDLLOVERRIDES", "winemenubuilder.exe=d", ";");
	}
}
*/


static LnWine* ln_wine_new(const gchar* path, LnArch bin, LnArch bin64)
{
	g_assert(path);

	LnWine* self = NULL;

	self = (LnWine*) g_malloc0(sizeof(LnWine));
	self->bin = bin;
	self->bin64 = bin64;
	self->path = g_strndup(path, (gsize) strlen(path));

	return self;
}


/* Factory method */
LnWine* ln_wine_create(const gchar* path)
{
	g_assert(path);

	LnWine* self = NULL;

	if (_is_valid(path))
	{
		LnArch bin = LN_ARCH_NULL;
		LnArch bin64 = LN_ARCH_NULL;

		bin = _get_executable_arch(path, "wine");
		bin64 = _get_executable_arch(path, "wine64");

		if (bin != LN_ARCH_NULL || bin64 != LN_ARCH_NULL)
		{
			self = ln_wine_new(path, bin, bin64);
		}
	}

	return self;
}


gboolean ln_wine_add_registry_key(const LnWine* self, const gchar* key, const gchar* name, const gchar* type, const gchar* data)
{
	g_assert(self);
	g_assert(key);
	g_assert(name);
	g_assert(data);

	g_autoptr (GStrvBuilder) builder = NULL;
	gboolean status = FALSE;

	builder = g_strv_builder_new();
	if (builder)
	{
		g_auto (GStrv) cmd = NULL;

		g_strv_builder_add_many(builder,
			ln_wine_get_bin_path(self), "reg", "add", key, "/v", name, "/t", type, "/d", data, "/f", NULL);

		cmd = g_strv_builder_end(builder);
		if (cmd)
		{
			status = ln_run_process(NULL , cmd, TRUE);
		}
	}

	return status;
}


gboolean ln_wine_delete_registry_key(const LnWine* self, const gchar* key, const gchar* name)
{
	g_autoptr (GStrvBuilder) builder = NULL;
	gboolean status = FALSE;

	builder = g_strv_builder_new();
	if (builder)
	{
		g_auto (GStrv) cmd = NULL;

		g_strv_builder_add_many(builder,
			ln_wine_get_bin_path(self), "reg", "delete", key, "/v", name, "/f", NULL);

		cmd = g_strv_builder_end(builder);
		if (cmd)
		{
			status = ln_run_process(NULL , cmd, TRUE);
		}
	}

	return status;
}


void ln_wine_free(LnWine* self)
{
	g_assert(self);

	g_free(self->path);

	g_free(self);
	self = NULL;
}


gchar* ln_wine_get_bin_path(const LnWine* self)
{
	g_assert(self);

	g_autofree gchar* exec = NULL;
	gchar* path = NULL;

	if ((exec = ln_wine_get_exec(self)))
	{
		path = g_build_filename(self->path, exec, NULL);
	}

	return path;
}


gchar* ln_wine_get_exec(const LnWine* self)
{
	g_assert(self);

	gchar* exec = NULL;

	if (_is_wow64(self) || self->bin == LN_ARCH_32BIT_MODE)
	{
		exec = g_strndup("wine", 4);
	}
	else if (self->bin == LN_ARCH_NULL && self->bin64 == LN_ARCH_64BIT_MODE)
	{
		exec = g_strndup("wine64", 6);
	}

	return exec;
}


gchar* ln_wine_get_registry_key(const LnWine* self, const gchar* key, const gchar* name)
{
	g_assert(self);
	g_assert(key);
	g_assert(name);

	g_autofree gchar* bin = NULL;
	gchar* value = NULL;

	bin = ln_wine_get_bin_path(self);
	if (bin)
	{
		g_autofree gchar* cmd = NULL;

		cmd = g_strdup_printf("%s reg query \"%s\" /v \"%s\"", bin, key, name);
		if (cmd)
		{
			value = ln_get_output_cmd(cmd);

			/*
			 * TODO Parse output to get registry value
			 * Unusable in current state
			 */
		}
	}

	return value;
}


gchar* ln_wine_get_version(const LnWine* self)
{
	g_assert(self);

	g_autofree gchar* bin = NULL;
	gchar* value = NULL;

	bin = ln_wine_get_bin_path(self);
	if (bin)
	{
		g_autofree gchar* cmd = NULL;

		cmd = g_strjoin(" ", bin, "--version", NULL);
		if (cmd && (value = ln_get_output_cmd(cmd)))
		{
			ln_remove_linefeed_to_end_string(&value);
		}
	}
	TRACE("version: %s", value);

	return value;
}


void ln_wine_configure_debug_runtime(const LnWine* self)
{
	g_assert(self);

	const gchar* env = NULL;

	env = g_getenv("LUNIONPLAY_LOG");
	if (env)
	{
		if (g_strcmp0(env, "1") == 0)
		{
			g_setenv("WINEDEBUG", "fixme-all", FALSE);
		}
		else if (g_strcmp0(env, "2") == 0)
		{
			g_setenv("WINEDEBUG", "warn+seh", FALSE);
		}
		else
		{
			g_setenv("WINEDEBUG", "+timestamp,+pid,+tid,+seh,+debugstr,+loaddll,+mscoree", FALSE);
		}
	}
	else
	{
		g_setenv("WINEDEBUG", "-all", FALSE);
	}
}


void ln_wine_init(const LnWine* self)
{
	g_setenv("WINEFSYNC", "1", FALSE);
	/* fallback when fsync don't support */
	g_setenv("WINEESYNC", "1", FALSE);

	ln_append_env("WINEDLLOVERRIDES", "winedebug.exe=d", ";");
	ln_append_env("WINEDLLOVERRIDES", "winemenubuilder.exe=d", ";");

	ln_wine_configure_debug_runtime(self);
	/* ln_wine_configure_gstreamer_runtime(self); */
}


gboolean ln_wine_make_prefix(const LnWine* self, const gchar* option)
{
	g_assert(self);

	g_autofree gchar* dlls = NULL;
	g_autofree gchar* dbg = NULL;
	gboolean status = FALSE;

	dlls = g_strdup(g_getenv("WINEDLLOVERRIDES"));
	g_setenv("WINEDLLOVERRIDES", "mscoree,mshtml,winemenubuilder.exe=d", TRUE);

	dbg = g_strdup(g_getenv("WINEDEBUG"));
	/* No overwrite, can be usefull for debugging */
	g_setenv("WINEDEBUG", "-all", FALSE);

	status = ln_wine_use_boot(self, option);
	ln_wine_wait_server(self);

	ln_reset_env("WINEDLLOVERRIDES", dlls);
	ln_reset_env("WINEDEBUG", dbg);

	return status;
}


gboolean ln_wine_use_boot(const LnWine* self, const gchar* option)
{
	g_assert(self);

	g_autofree gchar* bin = NULL;
	gboolean status = FALSE;

	if ((bin = ln_wine_get_bin_path(self)))
	{
		g_auto(GStrv) cmd = NULL;

		if ((cmd = ln_build_argv(bin, "wineboot.exe", option, NULL)))
		{
			status = ln_run_process(NULL, cmd, FALSE);
		}
	}

	return status;
}


gboolean ln_wine_use_server(const LnWine* self, const gchar* option)
{
	g_assert(self);

	g_autofree gchar* bin = NULL;
	gboolean status = FALSE;

	if ((bin = g_build_filename(self->path, "wineserver", NULL)))
	{
		g_auto(GStrv) cmd = NULL;

		if ((cmd = ln_build_argv(bin, option, NULL)))
		{
			status = ln_run_process(NULL, cmd, FALSE);
		}
	}

	return status;
}


gboolean ln_wine_wait_server(const LnWine* self)
{
	g_assert(self);

	return ln_wine_use_server(self , "-w");
}