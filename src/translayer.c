/*
 * translayer.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "translayer.h"

#include "arch.h"
#include "log.h"
#include "parser.h"
#include "utils.h"

#include <glib/gstdio.h>
#include <fcntl.h>
#include <gio/gio.h>



/*
 * TODO Need refactor code and using new style static functions
 */

static gchar* dxvk_dlls[] =        {"d3d9", "d3d10core", "d3d11", "dxgi", NULL};
static gchar* vkd3dproton_dlls[] = {"d3d12", "d3d12core", NULL};

static gchar*   ln_translayer_build_path_dll_backup (const gchar* path, const gchar* dll);
static gboolean ln_translayer_create_fake_dll       (const char* dll);
static gchar*   ln_translayer_get_lib_arch          (const gchar* path, LnTranslayer type, LnArch arch, gboolean is_integrated);
static gboolean ln_translayer_move_dll              (const char* src, const gchar* dest);



static gchar* _build_cache_path(LnTranslayer type, const gchar* path)
{
	g_assert(path);

	g_autofree gchar* dirname = NULL;

	if (type == DXVK)
	{
		dirname = g_strndup("dxvk_state_cache", 16);
	}
	if (type == VKD3DPROTON)
	{
		dirname = g_strndup("vkd3d_proton_shader_cache", 25);
	}

	return g_build_path(G_DIR_SEPARATOR_S, path, dirname, NULL);
}


static gchar* _enum_to_string(LnTranslayer type)
{
	gchar* str = NULL;

	if (type == DXVK)
	{
		str = g_strndup("dxvk", 4);
	}
	if (type == VKD3DPROTON)
	{
		str = g_strndup("vkd3dproton", 11);
	}

	return str;
}


static gchar* ln_translayer_convert_arch(LnTranslayer type, LnArch arch, gboolean is_integrated)
{
	gchar* ret = NULL;

	if (is_integrated)
	{
		if (arch == LN_ARCH_64BIT_MODE)
		{
			ret = g_strndup("x86_64", 6);
		}
		if (arch == LN_ARCH_32BIT_MODE)
		{
			ret = g_strndup("x86", 3);
		}
	}
	else
	{
		if (arch == LN_ARCH_64BIT_MODE)
		{
			ret = g_strndup("x64", 3);
		}
		if (arch == LN_ARCH_32BIT_MODE && type == DXVK)
		{
			ret = g_strndup("x32", 3);
		}
		if (arch == LN_ARCH_32BIT_MODE && type == VKD3DPROTON)
		{
			ret = g_strndup("x86", 3);
		}
	}

	return ret;
}



static gboolean ln_translayer_backup_dll(const gchar* path)
{
	g_assert(path);

	gboolean ret = FALSE;
	gboolean is_done = FALSE;
	g_autofree gchar* backup = NULL;

	backup = ln_translayer_build_path_dll_backup(path, NULL);
	if (backup && ! (g_file_test(path, G_FILE_TEST_EXISTS) && g_file_test(backup, G_FILE_TEST_EXISTS)))
	{
		// dll isn't' present into wine prefix, so create fake dll ".old_none"
		if (! g_file_test(path, G_FILE_TEST_EXISTS))
		{
			is_done = ln_translayer_create_fake_dll(backup);
		}
		// dll is present into wine prefix, so rename this dll into ".old"
		else
		{
			is_done = ln_translayer_move_dll(path, backup);
		}
	}

	if (is_done && g_file_test(path, G_FILE_TEST_EXISTS) && g_file_test(backup, G_FILE_TEST_EXISTS))
	{
		ret = TRUE;
	}

	return ret;
}


static gchar* ln_translayer_build_path_dll(const gchar* path, const gchar* dll)
{
	g_assert(path);
	g_assert(dll);

	g_autofree gchar* tmp = NULL;

	tmp = g_build_path("/", path, dll, NULL);

	return g_strconcat(tmp, ".dll", NULL);
}


static gchar* ln_translayer_build_path_dll_backup(const gchar* path, const gchar* dll)
{
	g_assert(path);

	gchar* ret = NULL;
	g_autofree gchar* tmp = NULL;

	if (! g_str_has_suffix(path, ".dll"))
	{
		tmp = ln_translayer_build_path_dll(path, dll);
	}
	else
	{
		tmp = g_strdup(path);
	}

	if (tmp)
	{
		ret = g_strconcat(tmp, ".old", NULL);
		if (! g_file_test(tmp, G_FILE_TEST_EXISTS))
		{
			g_autofree gchar* p_tmp = NULL;

			p_tmp = ret;
			g_free(ret);
			ret = g_strconcat(p_tmp, "_none", NULL);
		}
	}

	return ret;
}


static gboolean ln_translayer_create_fake_dll(const char* dll)
{
	g_assert(dll);

	gboolean ret = FALSE;
	g_autoptr(GFile) file = NULL;
	g_autoptr(GFileOutputStream) io_stream = NULL;
	g_autoptr(GError) error = NULL;

	file = g_file_new_for_path(dll);
	if (file)
	{
		io_stream = g_file_create(file, G_FILE_CREATE_NONE, NULL, &error);
		if (io_stream)
		{
			ret = TRUE;
			TRACE("-> %s", dll);
		}
	}

	return ret;
}


static gchar* ln_translayer_get_key(LnTranslayer type)
{
	gchar* ret = NULL;

	switch(type)
	{
		case DXVK:
			ret = g_strndup("dxvk", 4);
			break;
		case VKD3DPROTON:
			ret = g_strndup("vkd3d_proton", 12);
			break;
	}

	return ret;
}


static LnLib* ln_translayer_get_lib(LnTranslayer type, GKeyFile* cfg, const gchar* program_id)
{
	g_assert(program_id);

	gboolean is_int = FALSE;
	g_autofree gchar* key = NULL;
	g_autofree gchar* path = NULL;
	LnLib* ret = NULL;

	key = ln_translayer_get_key(type);
	if (key)
	{
		path = ln_parser_get_string(cfg, program_id, key);
		if (path)
		{
			g_autofree gchar* path64 = NULL;
			g_autofree gchar* path32 = NULL;

			path64 = ln_translayer_get_lib_arch(path, type, LN_ARCH_64BIT_MODE, is_int);
			path32 = ln_translayer_get_lib_arch(path, type, LN_ARCH_32BIT_MODE, is_int);

			if (path64 || path32)
			{
				ret = ln_lib_new(path32, path64);
			}
		}
	}

	return ret;
}


static gchar* ln_translayer_get_lib_arch(const gchar* path, LnTranslayer type, LnArch arch, gboolean is_integrated)
{
	g_assert(path);

	g_autofree gchar* _arch = NULL;
	gchar* ret = NULL;

	_arch = ln_translayer_convert_arch(type, arch, is_integrated);
	if (_arch)
	{
		ret = g_build_path("/", path, _arch, NULL);
		if (ret && ! g_file_test(ret, G_FILE_TEST_IS_DIR))
		{
			g_free(ret);
			ret = NULL;
		}
	}

	return ret;
}


static gboolean ln_translayer_install_dll(const gchar* src, const gchar* dest)
{
	g_assert(src);
	g_assert(dest);

	gboolean ret = FALSE;
	g_autoptr(GFile) source = NULL;
	g_autoptr(GFile) destination = NULL;
	g_autoptr(GError) error = NULL;

	source = g_file_new_for_path(src);
	destination = g_file_new_for_path(dest);

	if (source && destination)
	{
		ret = g_file_copy(source, destination, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error);
		if (ret)
		{
			TRACE("%s -> %s", src, dest);
		}
	}

	return ret;
}


static gboolean ln_translayer_install_lib(LnTranslayer type, const gchar* src_path, const gchar* wine_path)
{
	g_assert(src_path);
	g_assert(wine_path);

	gchar** dlls = NULL;
	gint nb_dlls = 0;
	gint count = 0;
	gboolean ret = FALSE;

	switch(type)
	{
		case DXVK:
			dlls = dxvk_dlls;
			nb_dlls = 4;
			break;
		case VKD3DPROTON:
			dlls = vkd3dproton_dlls;
			nb_dlls = 1;
			break;
	}

	for (gchar** tmp = dlls; *tmp; tmp++)
	{
		g_autofree gchar* src = ln_translayer_build_path_dll(src_path, *tmp);
		g_autofree gchar* dest = ln_translayer_build_path_dll(wine_path, *tmp);

		if (src && dest)
		{
			ln_translayer_backup_dll(dest);
			ln_translayer_install_dll(src, dest);
			count++;
		}
	}

	if (count == nb_dlls)
	{
		ret = TRUE;
	}

	return ret;
}


static gboolean ln_translayer_move_dll(const char* src, const gchar* dest)
{
	g_assert(src);
	g_assert(dest);

	gboolean ret = FALSE;
	g_autoptr(GFile) source = NULL;
	g_autoptr(GFile) destination = NULL;
	g_autoptr(GError) error = NULL;

	source = g_file_new_for_path(src);
	destination = g_file_new_for_path(dest);

	if (source && destination)
	{
		ret = g_file_move(source, destination, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error);
		if (ret)
		{
			TRACE("%s -> %s", src, dest);
		}
	}


	return ret;
}


/* TODO Need rework this function and pass to be public function
static gboolean ln_translayer_test_dll(const gchar* path, const gchar** dlls)
{
	g_assert(path);
	g_assert(dlls);

	gboolean ret = TRUE;

	for (const gchar** tmp = dlls; *tmp; tmp++)
	{
		g_autofree gchar* dll = NULL;
		g_autofree gchar* old = NULL;

		dll = g_build_path("/", path, *tmp, NULL);
		old = g_strconcat(dll, ".old", NULL);

		ret = ret && (g_file_test(path, G_FILE_TEST_EXISTS) &&
			g_file_test(old, G_FILE_TEST_EXISTS));
	}

	return ret;
}
*/


static void ln_translayer_setup_dxvk_runtime(const gchar* path)
{
	if (g_getenv("LUNION_LOG") == NULL)
	{
		g_setenv("DXVK_LOG_LEVEL", "none", FALSE);
	}
	TRACE("%s=%s", "DXVK_LOG_LEVEL", g_getenv("DXVK_LOG_LEVEL"));

	if (path)
	{
		g_setenv("DXVK_STATE_CACHE_PATH", path, FALSE);
		TRACE("DXVK_STATE_CACHE_PATH=%s", g_getenv("DXVK_STATE_CACHE_PATH"));
	}
}


static void ln_translayer_setup_vkd3d_proton_runtime(const gchar* path)
{
	if (g_getenv("LUNION_LOG") == NULL)
	{
		g_setenv("VKD3D_DEBUG", "none", FALSE);
	}
	TRACE("%s=%s", "VKD3D_DEBUG", g_getenv("VKD3D_DEBUG"));

	if (path)
	{
		g_setenv("VKD3D_SHADER_CACHE_PATH", path, FALSE);
		TRACE("VKD3D_SHADER_CACHE_PATH=%s", g_getenv("VKD3D_SHADER_CACHE_PATH"));
	}
}


LnLib* ln_translayer_get_dxvk(GKeyFile* cfg, const gchar* program_id)
{
	g_assert(program_id);

	return ln_translayer_get_lib(DXVK, cfg, program_id);
}


LnLib* ln_translayer_get_vkd3dproton(GKeyFile* cfg, const gchar* program_id)
{
	g_assert(program_id);

	return ln_translayer_get_lib(VKD3DPROTON, cfg, program_id);
}


void ln_translayer_install(const LnTranslayer type, const LnLib* src, const LnLib* dest)
{
	g_assert(src);
	g_assert(dest);

	const gchar* dst32 = NULL;
	const gchar* dst64 = NULL;

	dst64 = ln_lib_get_path(dest, LN_ARCH_64BIT_MODE);
	if (dst64)
	{
		ln_translayer_install_lib(type,
			ln_lib_get_path(src, LN_ARCH_64BIT_MODE),
			dst64);
	}

	dst32 = ln_lib_get_path(dest, LN_ARCH_32BIT_MODE);
	if (dst32)
	{
		ln_translayer_install_lib(type,
			ln_lib_get_path(src, LN_ARCH_32BIT_MODE),
			dst32);
	}
}


void ln_translayer_init(LnTranslayer type, const gchar* path)
{
	g_assert(path);

	g_autofree gchar* cache_path = NULL;
	g_autofree gchar* dlls = NULL;

	/* Need create manually directory */
	cache_path = _build_cache_path(type, path);
	if (! cache_path || (cache_path && g_mkdir_with_parents(cache_path, 0700) != 0))
	{
		g_autofree gchar* type_s = NULL;

		type_s = _enum_to_string(type);
		WARN("%s: Skipping cache path configuration", type_s);
	}

	if (type == DXVK)
	{
		ln_translayer_setup_dxvk_runtime(cache_path);
		dlls = g_strndup("d3d9,d3d10core,d3d11,dxgi=n", 27);
	}
	if (type == VKD3DPROTON)
	{
		ln_translayer_setup_vkd3d_proton_runtime(cache_path);
		dlls = g_strndup("d3d12,d3d12core=n", 17);
	}

	ln_append_env("WINEDLLOVERRIDES", dlls, ";");
	TRACE("%s=%s", "WINEDLLOVERRIDES", g_getenv("WINEDLLOVERRIDES"));
}