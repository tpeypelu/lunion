/*
 * log.c
 *
 * Copyright (C) 2021-2022 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "log.h"

#include <glib/gprintf.h>

#define SIZE 4096


void ln_log(const gchar* level, const gchar* format, ...)
{
	va_list args;
	gchar buffer[SIZE];

	va_start(args, format);
	g_vsnprintf(buffer, sizeof(buffer), format, args);
	va_end(args);

	if (g_strcmp0(level, "err"))
	{
		g_fprintf(stderr, "%s:: %s\n", level, buffer);
	}
	else
	{
		g_fprintf(stdout, "%s:: %s\n", level, buffer);
	}
}


void ln_trace(const gchar* path, const gint line, const gchar* format, ...)
{
	g_assert(path);

	va_list args;
	gchar buffer[SIZE];
	g_autofree gchar* file = NULL;

	if (g_getenv("LUNION_DEBUG") == NULL)
	{
		return;
	}

	va_start(args, format);
	g_vsnprintf(buffer, sizeof(buffer), format, args);
	va_end(args);

	file = g_path_get_basename(path);
	g_fprintf(stderr, "trace::[%s:%d] %s\n", file, line, buffer);
}