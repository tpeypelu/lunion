/*
 * parser.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __CONFIG__
#define __CONFIG__

#include "driver.h"
#include "program.h"
#include "wine.h"

#include <glib.h>



typedef GKeyFile LnParser;


LnDriver ln_parser_get_driver(LnParser* self);

gchar* ln_parser_get_string(LnParser* self, const gchar* group, const gchar* key);

LnWine* ln_parser_get_wine(LnParser* self, const gchar* group);

LnParser* ln_parser_create(void);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(LnParser, g_key_file_free)



#endif