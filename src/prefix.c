/*
 * prefix.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "prefix.h"

#include "log.h"
#include "translayer.h"
#include "utils.h"



struct _ln_prefix
{
	LnLib* lib;
	LnArch arch;
};


static gchar*        _build_library_path(const gchar* dirname);

static LnArch        _get_windows_arch(const gchar* path);

static LnLib*        _get_library(LnArch arch);

static gchar*        _get_library_path(const gchar* dirname);

static FILE*         _open_system_file(const gchar* path);

static gchar*        _search_arch_to_file(FILE** stream);

static LnPrefix* ln_prefix_new(LnLib* lib, LnArch arch);



static gchar* _build_library_path(const gchar* dirname)
{
	g_assert(dirname);

	const gchar* prefix = NULL;
	gchar* path = NULL;

	prefix = ln_prefix_get_path();
	if (prefix)
	{
		path = g_build_path(G_DIR_SEPARATOR_S, prefix, "drive_c", "windows", dirname, NULL);
	}

	return path;
}


static LnArch _get_windows_arch(const gchar* path)
{
	g_assert(path);

	FILE* stream = NULL;
	LnArch arch = LN_ARCH_NULL;

	stream = _open_system_file(path);
	if (stream)
	{
		g_autofree gchar* string = NULL;

		string = _search_arch_to_file(&stream);
		if (string)
		{
			arch = ln_arch_get_from_string(string);
		}

		fclose(stream);
		stream = NULL;
	}

	return arch;
}


static LnLib* _get_library(LnArch arch)
{
	g_autofree gchar* lib32 = NULL;
	g_autofree gchar* lib64 = NULL;
	LnLib* lib = NULL;

	if (arch == LN_ARCH_64BIT_MODE)
	{
		lib64 = _get_library_path("system32");
		lib32 = _get_library_path("syswow64");
	}
	if (arch == LN_ARCH_32BIT_MODE)
	{
		lib32 = _get_library_path("system32");
	}

	if (lib32 || lib64)
	{
		lib = ln_lib_new(lib32, lib64);
	}

	return lib;
}


static gchar* _get_library_path(const gchar* dirname)
{
	g_assert(dirname);

	gchar* path = NULL;

	path = _build_library_path(dirname);
	if (path && ! g_file_test(path , G_FILE_TEST_IS_DIR))
	{
		g_free(path);
		path = NULL;
	}

	return path;
}


static FILE* _open_system_file(const gchar* path)
{
	g_autofree gchar* file = NULL;
	FILE* stream = NULL;

	if ((file = g_build_filename(path, "system.reg", NULL)))
	{
		stream = g_fopen(file, "r");
		if (! stream)
		{
			ERR("Failed to open %s", file);
		}
	}

	return stream;
}


static gchar* _search_arch_to_file(FILE** stream)
{
	g_assert(stream);

	gchar buffer[4096];
	gchar* value = NULL;

	while (fgets(buffer, 4096, *stream))
	{
		if (g_str_has_prefix(buffer, "#arch"))
		{
			gsize lenght = strlen(buffer);

			value = g_strndup(buffer, lenght - 1);
			break;
		}
	}

	return value;
}


static LnPrefix* ln_prefix_new(LnLib* lib, LnArch arch)
{
	g_assert(lib);

	LnPrefix* self = NULL;

	self = (LnPrefix*) g_malloc0(sizeof(LnPrefix));
	self->lib = lib;
	self->arch = arch;

	return self;
}


gchar* ln_prefix_build_path(const gchar* id)
{
	g_assert(id);

	g_autofree gchar* basedir = NULL;
	gchar* path = NULL;

	basedir = ln_get_compatdata_path();
	if (basedir && g_mkdir_with_parents(basedir, 0700) == 0)
	{
		path = g_build_path(G_DIR_SEPARATOR_S, basedir, id, NULL);
	}
	else
	{
		ERR("Cannot create directory %s", basedir);
	}

	return path;
}


/* Factory method */
LnPrefix* ln_prefix_create(void)
{
	const gchar* prefix = NULL;
	LnArch arch = LN_ARCH_NULL;
	LnPrefix* self = NULL;

	if ((prefix = ln_prefix_get_path()))
	{
		arch = _get_windows_arch(prefix);
		if (arch != LN_ARCH_NULL)
		{
			LnLib* lib = NULL;

			lib = _get_library(arch);
			if (lib)
			{
				self = ln_prefix_new(lib, arch);
			}
		}
	}
	else
	{
		ERR("Unable to create wineprefix");
	}

	return self;
}


void ln_prefix_free(LnPrefix* self)
{
	g_assert(self);

	ln_lib_free(self->lib);
	self->lib = NULL;
	self->arch = LN_ARCH_NULL;

	g_free(self);
	self = NULL;
}


LnArch ln_prefix_get_arch(const LnPrefix* self)
{
	return self->arch;
}


const gchar* ln_prefix_get_path(void)
{
	return g_getenv("WINEPREFIX");
}


gboolean ln_prefix_setup(const LnPrefix* self, const LnLib* dxvk, const LnLib* vkdp)
{
	g_assert(self);

	gboolean status = TRUE;

	if (self->lib && dxvk)
	{
		ln_translayer_install(DXVK, dxvk, self->lib);
	}
	if (self->lib && vkdp)
	{
		ln_translayer_install(VKD3DPROTON, vkdp, self->lib);
	}

	return status;
}