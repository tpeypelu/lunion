/*
 * parser.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "parser.h"

#include "log.h"
#include "utils.h"

#include <glib/gstdio.h>



static gchar* _build_path(void)
{
	gchar* path = NULL;

	path = ln_get_env("config_file");
	if (! path)
	{
		g_autofree gchar* dir = NULL;

		dir = ln_get_user_config_path();
		if (dir)
		{
			path = g_build_filename(dir, "config.ini", NULL);
		}
	}

	return path;
}


static gchar* _parse_keyfile(LnParser* self, const gchar* group, const gchar* key)
{
	g_assert(self);
	g_assert(group);
	g_assert(key);

	gchar* value = NULL;

	if (g_key_file_has_group(self, group)
		&& g_key_file_has_key(self, group, key, NULL))
	{
		value = g_key_file_get_string(self, group, key, NULL);
		if (value && g_strcmp0(value, "") == 0)
		{
			g_free(value);
			value = NULL;
		}
		TRACE("[%s] %s=%s", group, key, value);
	}

	return value;
}


LnDriver ln_parser_get_driver(LnParser* self)
{
	g_autofree gchar* value = NULL;
	LnDriver id = LN_DRIVER_NULL;

	/* Expose only in application's group */
	value = ln_parser_get_string(self , "lunion" , "driver");
	if (value)
	{
		id = ln_driver_get_from_string(value);
	}

	return id;
}


gchar* ln_parser_get_string(LnParser* self, const gchar* group, const gchar* key)
{
	g_assert(key);

	gchar* value = NULL;

	value = ln_get_env(key);
	if (! value && self)
	{
		value = _parse_keyfile(self, group, key);
		if (! value && g_strcmp0(key, "command") != 0)
		{
			/* Trying to get the key's value in main group */
			value = _parse_keyfile(self, "lunion", key);
		}
	}

	return value;
}


LnWine* ln_parser_get_wine(LnParser* self, const gchar* group)
{
	g_autofree gchar* path = NULL;
	LnWine* wine = NULL;

	path = ln_parser_get_string(self, group, "wine_path");
	if (!path)
	{
		path = g_find_program_in_path("wineboot");
	}

	if (path)
	{
		TRACE("Loading Wine from %s", path);

		wine = ln_wine_create(path);
	}

	return wine;
}


LnParser* ln_parser_create(void)
{
	g_autofree gchar* path = NULL;
	LnParser* self = NULL;

	path = _build_path();
	if (path)
	{
		g_autoptr (GError) err = NULL;

		self = g_key_file_new();
		if (g_key_file_load_from_file(self, path, G_KEY_FILE_NONE, &err))
		{
			TRACE("Loading configuration from %s", path);
		}
		else
		{
			ERR("%s", err->message);

			g_key_file_free(self);
			self = NULL;
		}
	}

	return self;
}