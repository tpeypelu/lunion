# Introduction

Lunion is a Wine/DXVK/VKD3D-Proton wrapper which allows running Windows games on Linux.

The main goal is to offer an simple alternative to launch your favorite games without using Proton and outside Steam. You can use the Wine package of your Linux distribution (not perfect for gaming) or Lutris builds or Wine GE builds or else Wine TkG builds.

**This is still early development. Be aware it is a work in progress and from complete yet**


## Dependencies
* glib


## How to use

You can launch a program:
```
lunion programid
```

### Example with a GOG game
1. Download the offline backup game installer (`setup_xxx.exe`) that you want on your [account GOG](https://www.gog.com/en/account).
2. Install game in launching Lunion with a game id of your choice to `LUNION_PROGRAM_ID` env. variable and passing path game installer as argument :
```
LUNION_PROGRAM_ID=gameid lunion /path/to/setup_xxx.exe
```
3. During installation configuration, change game destination to `Z:\home\<user>\path\to\directory`. Otherwise, by default, the game will be installed in the Wine prefix, which is not at all ideal, especially if you want to delete the prefix without deleting game data.
4. Create/Add this lines in [ini file](#configuration):
```ini
[gameid]
command=/$HOME/path/to/directory/to/game.exe
```
5. Enjoy your game:
```
lunion gameid
```


## Installation

For obtaining the source, you can clone the repository https://framagit.org/IroAlexis/lunion:
```
git clone https://framagit.org/IroAlexis/lunion.git && cd lunion
```

### Building Lunion
```
$ meson setup build
$ meson compile -C build
```


## Configuration
Lunion use config file which can be used to set runtime configuration options.

### Basic package
* `$XDG_CONFIG_HOME/lunion/config.ini`
* `$HOME/.config/lunion/config.ini`

### Config file content
```ini
[lunion]
; wine binaries directory
wine_path = /path/to/wine/bin
; dxvk directory
dxvk = /path/to/dxvk
; vkd3d-proton directory
vkd3d_proton = /path/to/vkd3d-proton
; driver to use - options are: radv, nvidia or intel
driver = nvidia

[programid]
; use an another Wine for this program
wine_path = /path/to/another/wine/bin
; windows executable to launch for this program id
command = /path/to/program.exe
; add arguments for launching them with the program
command_args = -dx11,-launcher-skip
```

### Runtime Configuration Options
All of the below are runtime options. You can use normally the runtime options others programms (Wine, graphical driver, ...). Removing the option will revert to the previous behavior.
| Name                          | Value                   | Description |
| :---------------------------- |:----------------------- | :---------- |
| `LUNION_CONFIG_FILE`      | `/path/to/ini`          | Use an custom configuration file |
| `LUNION_DXVK`             | `/path/to/dxvk`         | Use an custom DXVK directory |
| `LUNION_PROGRAM_ID`       | `programid`             | Indicate an program id for Lunion when you install  program with his installer |
| `LUNION_USE_WINED3D`      | `1`                     | Use OpenGL-based wined3d instead of Vulkan-based DXVK for d3d11, d3d10, and d3d9 |
| `LUNION_USE_VKD3D`        | `1`                     | Use Vulkan-based vkd3d for d3d12 |
| `LUNION_VKD3D_PROTON`     | `/path/to/vkd3d-proton` | Use an custom VKD3D-Proton directory |
| `LUNION_WINE_PATH`        | `/path/to/wine/bin`     | Use an custom Wine binaries directory |



## Acknowledgements
* [Thibault](https://framagit.org/dysliked)
* [TkG](https://github.com/Tk-Glitch)
* [GloriousEggroll](https://github.com/GloriousEggroll)
* [Hans-Kristian Arntzen](https://github.com/HansKristian-Work)
* [doitsujin](https://github.com/doitsujin)
* [Joshua Ashton](https://github.com/Joshua-Ashton)
* [Blisto](https://github.com/Blisto91)
* [Wine](https://winehq.org)
* [Valve](https://github.com/ValveSoftware/Proton)
* [openglfreak](https://github.com/openglfreak)
* [Legendary](https://github.com/derrod/legendary)
* [Lutris](https://github.com/lutris)


## Licence
Available in [LICENSE](LICENSE) file<br>

**Copyright (C)** 2023 Alexis Peypelut alias IroAlexis
